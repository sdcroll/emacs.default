;; Store 'Custom' setings in a separate file (e.g custom-set-variables and custom-set-faces)
(setq custom-file (concat user-emacs-directory "custom.el"))
(load custom-file 'noerror)

;; Load the emacs-lisp source code blocks from 'config.org' (note: creates 'config.el')
(org-babel-load-file
 (expand-file-name
  "config.org"
  user-emacs-directory))
