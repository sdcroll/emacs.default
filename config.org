#+TITLE: Stephen Croll's GNU Emacs Config
#+OPTIONS: html-postamble:nil ^:nil
#+PROPERTY: header-args :eval no
#+OPTIONS: H:4
#+HTML_HEAD: <link rel="stylesheet" type="text/css" href="https://gongzhitaao.org/orgcss/org.css"/>

* MELPA

When installing new packages to an existing emacs install, you will
likely need to manually update the MELPA package lists:

#+begin_src
M-x package-refresh-contents
#+end_src

#+begin_src emacs-lisp
(require 'package)
(add-to-list 'package-archives
             '("melpa" . "https://melpa.org/packages/"))
(package-initialize)

(unless package-archive-contents
  (package-refresh-contents))
#+end_src

* Use-Package

- https://github.com/jwiegley/use-package

If you see ~use-package Failed to install~ errors, refresh the package
contents as described in the MELPA section above.

#+begin_src emacs-lisp
(unless (package-installed-p 'use-package)
  (package-install 'use-package))

(require 'use-package-ensure)
(setq use-package-always-ensure t)
#+end_src

* General Keybindings

#+begin_src emacs-lisp
(global-set-key "\C-cg" 'goto-line)
(global-set-key "\C-cw" 'what-line)
(global-set-key "\C-cr" 'copy-region-as-kill)
(global-set-key "\C-cc" 'compile)
(global-set-key "\C-t" 'delete-other-windows)
(global-set-key "\C-h" 'delete-backward-char)
(global-set-key "\eE" 'call-last-kbd-macro)

(global-set-key [M-up] 'previous-error)
(global-set-key [M-down] 'next-error)

(global-set-key "\es" 'shrink-window)
(global-set-key "\en" 'enlarge-window)
;; (global-set-key "\ep" 'dabbrev-expand)
#+end_src

* Visuals

** Theme

#+begin_src emacs-lisp
(use-package doom-themes
  :custom-face
  (font-lock-variable-name-face ((t (:foreground "#c897f4")))) ; light purple/pink
  (show-paren-match             ((t (:foreground "white" :background "#8f1296"))))
  :config
  (setq doom-themes-enable-bold t)   ; if nil, bold is universally disabled
  (setq doom-themes-enable-italic t) ; if nil, italics is universally disabled
  (load-theme 'doom-nord t))

(custom-set-faces '(org-block ((t (:background "#171E2C" :extend t))))) ; darker than #373E4C
#+end_src

** Modeline

#+begin_src emacs-lisp
(use-package doom-modeline
  :ensure t
  :init (doom-modeline-mode 1))
#+end_src

** Fonts

*** Emacs

#+begin_src emacs-lisp
(set-face-attribute 'default nil
  :font "Ubuntu Mono:pixelsize=14:foundry=DAMA:weight=normal:slant=normal:width=normal:spacing=100:scalable=true"
  :weight 'medium)
#+end_src

*** Match Emacs Font in Konsole

From M-x describe-font:

#+begin_example
name (opened by): -DAMA-Ubuntu Mono-normal-normal-normal-*-14-*-*-*-m-0-iso10646-1
       full name: Ubuntu Mono:pixelsize=14:foundry=DAMA:weight=normal:slant=normal:width=normal:spacing=100:scalable=true
       file name: /usr/share/fonts/truetype/ubuntu/UbuntuMono-R.ttf
            size: 14
          height: 15
 baseline-offset:  0
relative-compose:  0
  default-ascent:  0
          ascent: 12
         descent:  3
   average-width:  7
     space-width:  7
       max-width:  7
#+end_example

Edit desired profile, e.g. ~/.local/share/konsole/Profile\ 1.profile:

Change:

#+begin_example
Font=Ubuntu Mono,10,-1,5,50,0,0,0,0,0
#+end_example

To:

#+begin_example
Font=Ubuntu Mono,-1,14,5,50,0,0,0,0,3
#+end_example

References:
- [[https://stackoverflow.com/questions/64002615/is-the-format-of-qfont-tostring-documented-and-or-stable-across-qt-versions][Is the format of QFont.toString() documented and/or stable across Qt versions?]]

#+begin_example
// https://code.qt.io/cgit/qt/qtbase.git/tree/src/gui/text/qfont.cpp?h=5.15#n2070
QString QFont::toString() const
{
    const QChar comma(QLatin1Char(','));
    QString fontDescription = family() + comma +
        QString::number(     pointSizeF()) + comma +
        QString::number(      pixelSize()) + comma +
        QString::number((int) styleHint()) + comma +
        QString::number(         weight()) + comma +
        QString::number((int)     style()) + comma +
        QString::number((int) underline()) + comma +
        QString::number((int) strikeOut()) + comma +
        QString::number((int)fixedPitch()) + comma +
        QString::number((int)   false);

    QString fontStyle = styleName();
    if (!fontStyle.isEmpty())
        fontDescription += comma + fontStyle;

    return fontDescription;
}
#+end_example

- [[https://doc.qt.io/qt-6/qfont.html][QFont Class]]

** Background Color

#+begin_src emacs-lisp
(if (display-graphic-p)
    (set-face-background 'default "#000000")
  (set-face-background 'default "undefined"))
#+end_src

** Window Size / Menu Bar / Tool Bar

#+begin_src emacs-lisp
(if (display-graphic-p)
    (setq window-system-default-frame-alist
          '((x     (width . 106) (height . 30)
                   (menu-bar-lines . 1)
                   (tool-bar-lines . 0)))))
#+end_src

** Disable Blinking Cursor

#+begin_src emacs-lisp
(blink-cursor-mode -1)
#+end_src

** Icons

#+begin_src emacs-lisp
(use-package nerd-icons)
#+end_src

** Line Numbers

#+begin_src emacs-lisp
(global-display-line-numbers-mode t)
(setq-default display-line-numbers-width 4)
#+end_src

** Disable Electric Indent for All Major Modes

#+begin_src emacs-lisp
(add-hook 'after-change-major-mode-hook (lambda() (electric-indent-mode -1)))
#+end_src

** Neotree

- https://github.crookster.org/macOS-Emacs-26-display-line-numbers-and-me/#neotree-and-display-line-numbers-out-of-the-box

#+begin_src emacs-lisp
(use-package neotree
  :bind
  ("<f8>" . neotree-toggle)

  :config
  ;; needs package all-the-icons
  (setq neo-theme (if (display-graphic-p) 'icons 'arrow))

  ;; Disable line-numbers minor mode for neotree
  (add-hook 'neo-after-create-hook
            (lambda (&rest _) (display-line-numbers-mode -1)))

  ;; Every time when the neotree window is opened, let it find current
  ;; file and jump to node.
  (setq neo-smart-open t))
#+end_src

* Dashboard

#+begin_src emacs-lisp
;; (recentf-mode 1)
;; (setq recentf-max-menu-items 20)
;; (setq recentf-max-saved-items 20)
;; (run-at-time nil (* 5 60) 'recentf-save-list)

(use-package dashboard
  :init
  (setq dashboard-set-heading-icons t)
  (setq dashboard-set-file-icons t)
  (setq dashboard-startup-banner 'logo)
  (setq dashboard-center-content nil)
  ;; (setq dashboard-items '((recents . 20)))
  (setq dashboard-icon-type 'nerd-icons)

  :config
  (dashboard-setup-startup-hook)
  (setq dashboard-set-footer nil))
#+end_src

* Tramp

Retain remote path after login.

#+begin_src emacs-lisp
(with-eval-after-load "tramp" (add-to-list 'tramp-remote-path 'tramp-own-remote-path))
#+end_src

Tramp appears to use ~/bin/sh~ as the default shell on a remote host,
so be sure that the path you want is set correctly in that shell
(possibly via ~~/.profile~).

* Languages

** Go

*** Prerequisites

- https://github.com/golang/tools/blob/master/gopls/README.md
- https://github.com/golang/tools/blob/master/gopls/doc/emacs.md

Install the go tools gopls, godef (for jump to definition), and goimports as
follows:

#+begin_src sh
go install golang.org/x/tools/gopls@latest
go install github.com/rogpeppe/godef@latest
go install golang.org/x/tools/cmd/goimports@latest
#+end_src

Be sure to add something like this to your ~~/.profile~ to add the go
tools to your path so that Emacs can find them:

#+begin_src sh
export PATH=$PATH:/usr/local/go/bin
export PATH=$PATH:$(go env GOPATH)/bin
#+end_src

*** Go Mode

- [[https://www.youtube.com/watch?v=UFPD7icMoHY&t=2s][Emacs from Source Part 4: IDE Features with lsp-mode, company-mode, and go-mode]]
- https://gist.github.com/ntBre/34e3d2daad59040b4549cd8057f304c3

#+begin_src emacs-lisp
(require 'project)

(defun project-find-go-module (dir)
  (when-let ((root (locate-dominating-file dir "go.mod")))
    (cons 'go-module root)))

(cl-defmethod project-root ((project (head go-module)))
  (cdr project))

(add-hook 'project-find-functions #'project-find-go-module)

;; Optional: load other packages before eglot to enable eglot integrations.
(use-package company
  :bind (:map company-active-map
              ([return] . nil)
              ("RET"    . nil)
              ([tab]    . company-complete-selection)
              ("TAB"    . company-complete-selection)))

(use-package yasnippet)

(use-package go-mode
  :hook
  (go-mode . company-mode)
  :config
  (setq gofmt-command "goimports")
  (add-hook 'go-mode-hook (lambda () (setq tab-width 4)))
  (add-hook 'before-save-hook 'gofmt-before-save)
)
#+end_src

Note that to use ~gofmt~ and other commands on remote hosts, we need
to ensure we can find said programs. The [[*Tramp][Tramp]] section above will
retain the remote path after login (this presumes ~gofmt~ and such is
actually in your path on the remote host).

Another option is to explicitly set the path with something like:
~(setq gofmt-command "/usr/local/go/bin/gofmt")~.

Also note that adding a pathname to ~tramp-remote-path~ does /not/
work for some reason.

** Protobuf

#+begin_src emacs-lisp
(use-package protobuf-mode)
#+end_src

** Terraform

Note: Terraform needs to be installed locally for this to work.

#+begin_src emacs-lisp
(use-package terraform-mode)

(add-hook 'terraform-mode-hook 'terraform-format-on-save-mode)
#+end_src

** Dockerfile

#+begin_src emacs-lisp
(use-package dockerfile-mode)
#+end_src

** Markups

#+begin_src emacs-lisp
(use-package yaml-mode)
(use-package json-mode)
(use-package markdown-mode)
#+end_src

** Python

#+begin_src emacs-lisp
(add-hook 'python-mode-hook
          (lambda () (setq python-indent-offset 4)))
#+end_src

** C++

Note in c/c++ modes, use the ~c-show-syntactic-information~ function
to show syntactic information for current line.

You can also set the ~c-debug-parse-state~ variable as well for added
debug info.

#+begin_src emacs-lisp
;; (setq c-debug-parse-state t)

(defun my-c-mode-common-hook ()
  (c-add-style "scroll"
               '((c-basic-offset . 4)
                 (c-comment-only-line-offset . 0)
                 (c-offsets-alist . ((statement-block-intro . +)
                                     (substatement-open . 0)
                                     (label . 0)
                                     (statement-cont . +)
                                     (inline-open . 0)
                                     (inline-close . 0)
                                     (namespace-open . 0)
                                     (namespace-close . 0)
                                     (innamespace . c-lineup-whitesmith-in-block)
                                     ))))

  (c-set-style "scroll"))
  ;; (c-set-style "stroustrup")
(add-hook 'c-mode-common-hook 'my-c-mode-common-hook)
#+end_src

** Eglot

#+begin_src emacs-lisp
(use-package eglot
  :config
  (add-hook 'go-mode-hook 'eglot-ensure)
)
#+end_src

** Misc

Limit comments and documentation to 80 columns (instead of the default 70) when
running M-x fill-paragraph.

#+begin_src emacs-lisp
(setq-default fill-column 80)
#+end_src

* Magit

#+begin_src emacs-lisp
;;; (use-package magit)
#+end_src

* Org Mode

** Always Unfold Org Files

#+begin_src emacs-lisp
(setq org-startup-folded nil)
#+end_src

** Markdown

#+begin_src emacs-lisp
(require 'ox-md)
#+end_src

- https://github.com/larstvei/ox-gfm

#+begin_src emacs-lisp
(eval-after-load "org"
  '(require 'ox-gfm nil t))
#+end_src

** Jira Markup

- https://github.com/stig/ox-jira.el

#+begin_src emacs-lisp
(use-package ox-jira)
#+end_src

** Source Code Block Tag Expansion

Org-tempo is a package that allows for '<s' followed by TAB to expand
to a begin_src tag.  Other expansions available include:

| Typing the below + TAB | Expands to ...                          |
|------------------------+-----------------------------------------|
| <a                     | '#+BEGIN_EXPORT ascii' … '#+END_EXPORT  |
| <c                     | '#+BEGIN_CENTER' … '#+END_CENTER'       |
| <C                     | '#+BEGIN_COMMENT' … '#+END_COMMENT'     |
| <e                     | '#+BEGIN_EXAMPLE' … '#+END_EXAMPLE'     |
| <E                     | '#+BEGIN_EXPORT' … '#+END_EXPORT'       |
| <h                     | '#+BEGIN_EXPORT html' … '#+END_EXPORT'  |
| <l                     | '#+BEGIN_EXPORT latex' … '#+END_EXPORT' |
| <q                     | '#+BEGIN_QUOTE' … '#+END_QUOTE'         |
| <s                     | '#+BEGIN_SRC' … '#+END_SRC'             |
| <v                     | '#+BEGIN_VERSE' … '#+END_VERSE'         |

#+begin_src emacs-lisp
(use-package org-tempo
  :ensure nil) ;; tell use-package not to try to install org-tempo since it's already there.
#+end_src

** Source Code Block Syntax Highlighting

We want the same syntax highlighting in source blocks as in the native language files.

#+begin_src emacs-lisp
(setq org-src-fontify-natively t
    org-src-tab-acts-natively t
    org-confirm-babel-evaluate nil
    org-edit-src-content-indentation 0)
#+end_src

* Version Control

** Make Backups for Files Under Version Control

#+begin_src emacs-lisp
(setq vc-make-backup-files "true")
#+end_src

** Disable Version Control Hooks

Uncomment if using git over SSHFS or NFS, as it is horribly slow.

#+begin_src emacs-lisp
;; (setq vc-handled-backends ())
#+end_src

* Various Stuff

** Verify Quit

#+begin_src emacs-lisp
(setq confirm-kill-emacs (quote y-or-n-p))
#+end_src

** Turn Off Key Binding Suggestions

#+begin_src emacs-lisp
(setq suggest-key-bindings nil)
#+end_src

** Restore pre-Emacs 24 yank behavior

- http://www.emacswiki.org/emacs/CopyAndPaste

#+begin_src emacs-lisp
;; (setq select-active-regions nil)
;; (setq mouse-drag-copy-region t)
;; (setq x-select-enable-primary t)
;; (setq x-select-enable-clipboard nil)
;; (global-set-key [mouse-2] 'mouse-yank-primary)

;; ;; apparently needed for emacs 25.1
;; (setq select-enable-clipboard t)
#+end_src

** Disable Auto DOS Mode

#+begin_src emacs-lisp
(setq-default inhibit-eol-conversion t)
#+end_src

** Line Motion

Restore historical behavior of the C-n and C-p line-motion commands.
As of 23.1, these commands take continued lines and variable-width
characters into account, screwing up the way I use macros.

#+begin_src emacs-lisp
(setq-default line-move-visual nil)
#+end_src

** Whitespace Match

Restore historical literal whitespace match. Otherwise searching for
two spaces matches one space.

#+begin_src emacs-lisp
(setq search-whitespace-regexp nil)
#+end_src

** Word Expansion

#+begin_src emacs-lisp
(setq dabbrev-case-fold-search nil)
#+end_src

** Indentation

Make indentation use spaces and not tabs.

#+begin_src emacs-lisp
(setq-default indent-tabs-mode nil)
#+end_src

** Strip ^M's in Shell Mode

Get rid of those annoying ^M's in emacs shell mode

#+begin_src emacs-lisp
(add-hook 'comint-output-filter-functions 'comint-strip-ctrl-m)
#+end_src

** Browser

#+begin_src emacs-lisp
;; org export
(setenv "BROWSER" "/usr/bin/firefox")
;; everything else
(setq browse-url-browser-function 'browse-url-firefox)
#+end_src

** Compilation / Out-of-Source Builds

Similar to recompile, but does not necessarily include the current
buffer, as it switches directly to the last compilation buffer.

Intended to be used with out-of-source builds (e.g. cmake).

#+begin_src emacs-lisp
(require 'compile)

(global-set-key [f5] (lambda ()
                       (interactive)
                       (and (set-buffer "*compilation*") (recompile))))
#+end_src

** Revert Buffer Without Query

#+begin_src emacs-lisp
(defun my-revert-buffer ()
  "Revert the current buffer without query"
  (interactive nil)
  (revert-buffer nil t nil)
  (message "Buffer reverted."))
#+end_src

** Grep Buffers

- [[https://www.emacswiki.org/emacs/grep-buffers.el]]

#+begin_src emacs-lisp
(load (concat user-emacs-directory "grep-buffers"))
#+end_src

** ws-butler

ws-butler -- an unobtrusive way to trim spaces from end of line

- https://github.com/lewang/ws-butler

- https://gitlab.com/skybert/my-little-friends/-/blob/2022-emacs-from-scratch/emacs/.emacs

#+begin_src emacs-lisp
;; (use-package ws-butler
;;   :init
;;   (setq ws-butler-keep-whitespace-before-point nil)
;;   :config
;;   (ws-butler-global-mode))
#+end_src
